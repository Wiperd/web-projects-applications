package com.AnatoliiK.MuzCamp.Persistence.Service;

import com.AnatoliiK.MuzCamp.Persistence.Entity.Comment;
import com.AnatoliiK.MuzCamp.Persistence.Entity.Person;
import com.AnatoliiK.MuzCamp.Persistence.Entity.Publication;

import java.util.Optional;

public interface CommentService {
    Optional<Comment> findCommentByPublicationAndPerson(Publication publication, Person person);
    Optional<Comment> findCommentById(Integer id);
    Comment addComment(Comment comment);

    boolean deleteById(Integer id);
}
