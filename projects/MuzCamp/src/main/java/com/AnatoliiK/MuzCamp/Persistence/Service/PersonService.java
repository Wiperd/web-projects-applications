package com.AnatoliiK.MuzCamp.Persistence.Service;

import com.AnatoliiK.MuzCamp.Persistence.Entity.Person;

import java.util.List;
import java.util.Optional;

public interface PersonService {
    Person addPerson(Person person);
    Person editPerson(Person person);
    Optional<Person> findById(Integer id);
    List<Person> getAll();
    Optional<Person> findByLogin(String login);
    List<Person> findByUsername(String username);

}
