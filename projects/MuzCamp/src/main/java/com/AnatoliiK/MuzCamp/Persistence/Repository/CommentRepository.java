package com.AnatoliiK.MuzCamp.Persistence.Repository;

import com.AnatoliiK.MuzCamp.Persistence.Entity.Comment;
import com.AnatoliiK.MuzCamp.Persistence.Entity.Person;
import com.AnatoliiK.MuzCamp.Persistence.Entity.Publication;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface CommentRepository extends JpaRepository<Comment, Integer> {
    Optional<Comment> findByPublicationBeanAndPersonBean(Publication publication, Person person);
    Optional<Comment> findById(Integer id);
    void deleteById(Integer id);
}
