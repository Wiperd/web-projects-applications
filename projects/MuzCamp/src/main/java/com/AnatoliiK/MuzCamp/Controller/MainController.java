package com.AnatoliiK.MuzCamp.Controller;

import com.AnatoliiK.MuzCamp.Persistence.Entity.Genre;
import com.AnatoliiK.MuzCamp.Persistence.Entity.Person;
import com.AnatoliiK.MuzCamp.Persistence.Entity.Publication;
import com.AnatoliiK.MuzCamp.Persistence.Role;
import com.AnatoliiK.MuzCamp.Persistence.Service.Impl.GenreServiceImpl;
import com.AnatoliiK.MuzCamp.Persistence.Service.Impl.PersonServiceImpl;
import com.AnatoliiK.MuzCamp.Persistence.Service.Impl.PublicationServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import javax.validation.Valid;
import java.awt.print.Pageable;
import java.util.*;

@Controller
public class MainController {
    @Autowired
    private PersonServiceImpl personService;

    @Autowired
    private PublicationServiceImpl publicationService;

    @Autowired
    private GenreServiceImpl genreService;

    @GetMapping("/")
    public ModelAndView getMainPage(){
        ModelAndView modelAndView = new ModelAndView("mainpage");
        PageRequest pageOf5 = PageRequest.of(0, 5);

        Page<Object[]> topPubPage = publicationService.getAllAndSortBySales(pageOf5);
        List<Publication> topPub = new ArrayList<>();
        for (Object[] objects : topPubPage) {
            topPub.add((Publication) objects[0]);
        }
        modelAndView.addObject("topOverall", topPub);

        List<Genre> genres = genreService.getAll();
        Map<String, List<Publication>> topPubByGenres = new HashMap<>();
        for (Genre genre : genres) {
            List<Publication> topPubByGenre = new ArrayList<>();
            Page<Object[]> topPubByGenrePage = publicationService.findByGenreIdAndSortBySales(genre.getId(), pageOf5);
            for (Object[] objects : topPubByGenrePage) {
                topPubByGenre.add((Publication) objects[0]);
            }
            topPubByGenres.put(genre.getName(), topPubByGenre);
        }
        modelAndView.addObject("topByGenre", topPubByGenres);
        modelAndView.addObject("currentPerson", getCurrentPerson());
        return modelAndView;
    }

    @GetMapping("/signup")
    public ModelAndView getSignupForm(){
        ModelAndView modelAndView;
        Optional<Person> currentPerson = getCurrentPerson();
        if(currentPerson.isEmpty()){
            modelAndView = new ModelAndView("signup");
        } else{
            modelAndView = new ModelAndView("redirect:/");
        }
        modelAndView.addObject("currentPerson", currentPerson);
        return modelAndView;
    }

    @PostMapping("/signup")
    public ModelAndView addPerson(@Valid Person person, BindingResult bindingResult){
        ModelAndView modelAndView;
        Optional<Person> currentPerson = getCurrentPerson();
        if(currentPerson.isEmpty()){
            if(bindingResult.hasErrors()){
                String error = bindingResult.getFieldErrors().get(0).getDefaultMessage();
                modelAndView = new ModelAndView("signup");
                modelAndView.addObject("error", error);
            } else if(personService.findByLogin(person.getLogin()).isPresent()){
                String error = "Пользователь с таким логином уже существует";
                modelAndView = new ModelAndView("signup");
                modelAndView.addObject("error", error);
            } else{
                person.setRole_(Role.USER);
                personService.addPerson(person);
                modelAndView = new ModelAndView("redirect:/login");
            }
        } else{
            modelAndView = new ModelAndView("redirect:/");
        }
        modelAndView.addObject("currentPerson", currentPerson);
        return modelAndView;
    }

    @GetMapping("/login")
    public ModelAndView getLoginForm(@RequestParam(value = "error", defaultValue = "false") Boolean error){
        ModelAndView modelAndView;
        Optional<Person> currentPerson = getCurrentPerson();
        if(currentPerson.isEmpty()){
            modelAndView = new ModelAndView("login");
        }
        else{
            modelAndView = new ModelAndView("redirect:/");
        }
        modelAndView.addObject("currentPerson", currentPerson);
        modelAndView.addObject("error", error);
        return modelAndView;
    }

    @GetMapping("/search")
    public ModelAndView getSearchResults(@RequestParam(value = "query", defaultValue = "") String searchStr){
        ModelAndView modelAndView = new ModelAndView("search");
        List<Person> personList = personService.findByUsername(searchStr);
        List<Publication> publicationList = publicationService.findByTitle(searchStr);
        boolean noResult;
        if(personList.isEmpty() && publicationList.isEmpty()){
            noResult = true;
        } else {
            noResult = false;
        }
        modelAndView.addObject("noResult", noResult);
        modelAndView.addObject("searchStr", searchStr);
        modelAndView.addObject("personList", personList);
        modelAndView.addObject("publicationList", publicationList);

        Optional<Person> currentPerson = getCurrentPerson();
        modelAndView.addObject("currentPerson", currentPerson);
        return modelAndView;
    }

    public Optional<Person> getCurrentPerson(){
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        Optional<Person> currentPerson;
        if(authentication == null){
            currentPerson = Optional.empty();
        }
        else{
            currentPerson = personService.findByLogin(authentication.getName());
        }
        return currentPerson;
    }
}
