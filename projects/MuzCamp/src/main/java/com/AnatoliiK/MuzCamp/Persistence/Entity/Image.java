package com.AnatoliiK.MuzCamp.Persistence.Entity;

import javax.persistence.*;
import java.sql.Blob;
import java.util.List;

@Entity
@Table (name = "Image")
public class Image {
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    private Integer id;

    @Lob
    private Blob content;

    private String extension;


    @OneToMany(mappedBy="imageBean")
    private List<Person> persons;

    @OneToMany(mappedBy="imageBean")
    private List<Publication> publications;

    public Image() {
    }

    public Integer getId() {
        return this.id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Blob getContent() {
        return this.content;
    }

    public void setContent(Blob content) {
        this.content = content;
    }

    public String getExtension() {
        return this.extension;
    }

    public void setExtension(String extension) {
        this.extension = extension;
    }

    public List<Person> getPersons() {
        return this.persons;
    }

    public void setPersons(List<Person> persons) {
        this.persons = persons;
    }

    public Person addPerson(Person person) {
        getPersons().add(person);
        person.setImageBean(this);

        return person;
    }

    public Person removePerson(Person person) {
        getPersons().remove(person);
        person.setImageBean(null);

        return person;
    }

    public List<Publication> getPublications() {
        return this.publications;
    }

    public void setPublications(List<Publication> publications) {
        this.publications = publications;
    }

    public Publication addPublication(Publication publication) {
        getPublications().add(publication);
        publication.setImageBean(this);

        return publication;
    }

    public Publication removePublication(Publication publication) {
        getPublications().remove(publication);
        publication.setImageBean(null);

        return publication;
    }
}
