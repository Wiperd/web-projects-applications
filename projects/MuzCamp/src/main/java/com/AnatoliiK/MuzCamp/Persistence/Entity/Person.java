package com.AnatoliiK.MuzCamp.Persistence.Entity;

import com.AnatoliiK.MuzCamp.Persistence.Role;

import javax.persistence.*;
import javax.validation.constraints.Max;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.Date;
import java.util.List;

@Entity
@Table (name = "Person")
public class Person {
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    private Integer id;


    public Person(String login, String password, String username, Role role_){
        this.login = login;
        this.password = password;
        this.username = username;
        this.role_ = role_;
    }

    private String information;

    @NotBlank(message = "Логин не может быть пустым")
    @Size(max = 30, message = "Длина логина не может быть меньше 30 символов")
    private String login;

    @NotNull(message = "Пароль не может быть пустым")
    @Size(max = 30, message = "Пароль не может быть больше 30 символов")
    private String password;

    private Integer paycheck;

    @Enumerated(EnumType.STRING)
    private Role role_;

    @NotBlank(message = "Имя не может быть пустым")
    @Size(max = 50, message = "Имя не может быть больше 50 символов")
    private String username;

    @OneToMany(mappedBy="personBean")
    private List<Comment> comments;

    @ManyToOne
    @JoinColumn(name="image")
    private Image imageBean;

    @OneToMany(mappedBy="personBean")
    private List<Publication> publications;

    @OneToMany(mappedBy="personBean")
    private List<Purchasenotice> purchasenotices;

    @OneToMany(mappedBy="personBean")
    private List<Collectible> collectibles;

    @OneToMany(mappedBy="personBean")
    private List<WishItem> wishItems;

    @OneToMany(mappedBy="person")
    private List<Subscription> subscriptions1;

    @OneToMany(mappedBy="subscriber")
    private List<Subscription> subscriptions2;

    public Person() {
    }

    public Integer getId() {
        return this.id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getInformation() {
        return this.information;
    }

    public void setInformation(String information) {
        this.information = information;
    }

    public String getLogin() {
        return this.login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return this.password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Integer getPaycheck() {
        return this.paycheck;
    }

    public void setPaycheck(Integer paycheck) {
        this.paycheck = paycheck;
    }

    public Role getRole_() {
        return this.role_;
    }

    public void setRole_(Role role_) {
        this.role_ = role_;
    }

    public String getUsername() {
        return this.username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public List<Comment> getComments() {
        return this.comments;
    }

    public void setComments(List<Comment> comments) {
        this.comments = comments;
    }

    public Comment addComment(Comment comment) {
        getComments().add(comment);
        comment.setPersonBean(this);

        return comment;
    }

    public Comment removeComment(Comment comment) {
        getComments().remove(comment);
        comment.setPersonBean(null);

        return comment;
    }

    public Image getImageBean() {
        return this.imageBean;
    }

    public void setImageBean(Image imageBean) {
        this.imageBean = imageBean;
    }

    public List<Publication> getPublications() {
        return this.publications;
    }

    public void setPublications(List<Publication> publications) {
        this.publications = publications;
    }

    public Publication addPublication(Publication publication) {
        getPublications().add(publication);
        publication.setPersonBean(this);

        return publication;
    }

    public Publication removePublication(Publication publication) {
        getPublications().remove(publication);
        publication.setPersonBean(null);

        return publication;
    }

    public List<Purchasenotice> getPurchasenotices() {
        return this.purchasenotices;
    }

    public void setPurchasenotices(List<Purchasenotice> purchasenotices) {
        this.purchasenotices = purchasenotices;
    }

    public Purchasenotice addPurchasenotice(Purchasenotice purchasenotice) {
        getPurchasenotices().add(purchasenotice);
        purchasenotice.setPersonBean(this);

        return purchasenotice;
    }

    public Purchasenotice removePurchasenotice(Purchasenotice purchasenotice) {
        getPurchasenotices().remove(purchasenotice);
        purchasenotice.setPersonBean(null);

        return purchasenotice;
    }

    public List<Collectible> getCollectibles() {
        return this.collectibles;
    }

    public void setCollectibles(List<Collectible> collectibles) {
        this.collectibles = collectibles;
    }

    public Collectible addCollectible(Collectible collectible) {
        getCollectibles().add(collectible);
        collectible.setPersonBean(this);

        return collectible;
    }

    public Collectible removeCollectible(Collectible collectible) {
        getCollectibles().remove(collectible);
        collectible.setPersonBean(null);

        return collectible;
    }

    public List<WishItem> getWishItems() {
        return this.wishItems;
    }

    public void setWishItems(List<WishItem> wishItems) {
        this.wishItems = wishItems;
    }

    public WishItem addWishItem(WishItem wishItem) {
        getWishItems().add(wishItem);
        wishItem.setPersonBean(this);

        return wishItem;
    }

    public WishItem removeWishItem(WishItem wishItem) {
        getWishItems().remove(wishItem);
        wishItem.setPersonBean(null);

        return wishItem;
    }

    public List<Subscription> getSubscriptions1() {
        return this.subscriptions1;
    }

    public void setSubscriptions1(List<Subscription> subscriptions1) {
        this.subscriptions1 = subscriptions1;
    }

    public Subscription addSubscriptions1(Subscription subscriptions1) {
        getSubscriptions1().add(subscriptions1);
        subscriptions1.setPerson(this);

        return subscriptions1;
    }

    public Subscription removeSubscriptions1(Subscription subscriptions1) {
        getSubscriptions1().remove(subscriptions1);
        subscriptions1.setPerson(null);

        return subscriptions1;
    }

    public List<Subscription> getSubscriptions2() {
        return this.subscriptions2;
    }

    public void setSubscriptions2(List<Subscription> subscriptions2) {
        this.subscriptions2 = subscriptions2;
    }

    public Subscription addSubscriptions2(Subscription subscriptions2) {
        getSubscriptions2().add(subscriptions2);
        subscriptions2.setSubscriber(this);

        return subscriptions2;
    }

    public Subscription removeSubscriptions2(Subscription subscriptions2) {
        getSubscriptions2().remove(subscriptions2);
        subscriptions2.setSubscriber(null);

        return subscriptions2;
    }

}
