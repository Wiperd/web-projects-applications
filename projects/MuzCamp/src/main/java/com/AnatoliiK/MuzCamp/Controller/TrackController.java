package com.AnatoliiK.MuzCamp.Controller;


import com.AnatoliiK.MuzCamp.Persistence.Entity.Image;
import com.AnatoliiK.MuzCamp.Persistence.Entity.Track;
import com.AnatoliiK.MuzCamp.Persistence.Service.Impl.TrackServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.nio.file.Files;
import java.nio.file.Paths;

@Controller
@RequestMapping("/track")
public class TrackController {
    @Autowired
    private TrackServiceImpl trackService;

    private final String trackDir = "C:\\some\\trackdir\\";

    @GetMapping("/{id}")
    @ResponseBody
    public HttpEntity<byte []> loadTrackById(@PathVariable int id) throws Exception{
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(new MediaType("audio", "mp3"));
        byte[] trackContent = Files.readAllBytes(Paths.get(trackDir + id + ".mp3"));
        headers.setContentLength(trackContent.length);
        return new HttpEntity<byte[]>(trackContent, headers);
    }
}
