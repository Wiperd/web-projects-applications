package com.AnatoliiK.MuzCamp.Controller;

import com.AnatoliiK.MuzCamp.Persistence.Entity.*;
import com.AnatoliiK.MuzCamp.Persistence.Role;
import com.AnatoliiK.MuzCamp.Persistence.Service.Impl.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.ServletContext;
import java.util.List;
import java.util.Optional;

@Controller
@RequestMapping("/user")
public class UserController {
    @Autowired
    private PersonServiceImpl personService;

    @Autowired
    private ImageServiceImpl imageService;

    @Autowired
    private SubscriptionServiceImpl subscriptionService;

    @Autowired
    private PurchasenoticeServiceImpl purchasenoticeService;

    @Autowired
    private PublicationServiceImpl publicationService;

    @Autowired
    private WishItemServiceImpl wishItemService;

    @GetMapping("/{id}")
    public ModelAndView getUserMain(@PathVariable int id){
        Optional<Person> personOptional = personService.findById(id);
        ModelAndView modelAndView;
        if(personOptional.isPresent()){
            modelAndView = new ModelAndView("user-main");
            modelAndView.addObject("person", personOptional.get());
        } else{
            modelAndView = new ModelAndView("redirect:/");
            return modelAndView;
        }

        Optional<Person> currentPerson = getCurrentPerson();
        boolean subscribed = false;
        if (currentPerson.isPresent() && subscriptionService.findByPersonAndSubscriber(personOptional.get(), currentPerson.get()).isPresent()){
            subscribed = true;
        }
        modelAndView.addObject("subscribed", subscribed);
        modelAndView.addObject("currentPerson", currentPerson);
        return modelAndView;
    }

    @GetMapping("/settings")
    public ModelAndView getSettings(){
        Optional<Person> currentPerson = getCurrentPerson();
        ModelAndView modelAndView;
        if(currentPerson.isPresent()){
            modelAndView = new ModelAndView("user-settings");
        } else {
            modelAndView = new ModelAndView("redirect:/");
        }

        modelAndView.addObject("currentPerson", currentPerson);
        return modelAndView;
    }

    @PostMapping("/settings/edit")
    public ModelAndView editSettings(@RequestParam(name = "info") String information){
        ModelAndView modelAndView = new ModelAndView("redirect:/user/settings");
        Optional<Person> currentPerson = getCurrentPerson();
        if(currentPerson.isPresent()){
            currentPerson.get().setInformation(information);
            personService.editPerson(currentPerson.get());
        }
        return modelAndView;
    }

    @PostMapping("/settings/uploadimage")
    public ModelAndView uploadImage(@RequestParam("image")MultipartFile file){
        Optional<Person> currentPerson = getCurrentPerson();
        ModelAndView modelAndView;
        if(currentPerson.isPresent()){
            modelAndView = new ModelAndView("user-settings");
            if(file.isEmpty()){

            } else {
                String imgExtension = file.getOriginalFilename().split("\\.")[1];
                Image image = null;
                try {
                    image = imageService.addImageFromFile(file);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                currentPerson.get().setImageBean(image);
                personService.editPerson(currentPerson.get());
            }
        } else {
            modelAndView = new ModelAndView("redirect:/");
        }

        modelAndView.addObject("currentPerson", currentPerson);
        return modelAndView;
    }

    @GetMapping("/{id}/collection")
    public ModelAndView getCollection(@PathVariable int id){
        ModelAndView modelAndView;
        Optional<Person> personOptional = personService.findById(id);
        if(personOptional.isPresent()){
            modelAndView = new ModelAndView("user-collection");
            modelAndView.addObject("person", personOptional.get());
        } else {
            modelAndView = new ModelAndView("redirect:/");
            return modelAndView;
        }
        Optional<Person> currentPerson = getCurrentPerson();
        modelAndView.addObject("currentPerson", currentPerson);
        return modelAndView;
    }

    @GetMapping("/{id}/publications")
    public ModelAndView getPublications(@PathVariable int id){
        ModelAndView modelAndView;
        Optional<Person> personOptional = personService.findById(id);
        if(personOptional.isPresent()){
            modelAndView = new ModelAndView("user-publications");
            modelAndView.addObject("person", personOptional.get());
        } else {
            modelAndView = new ModelAndView("redirect:/");
            return modelAndView;
        }
        Optional<Person> currentPerson = getCurrentPerson();
        modelAndView.addObject("currentPerson", currentPerson);
        return modelAndView;
    }

    @PostMapping("/{id}/xsubscribe")
    public ModelAndView changeSubscriptionStatus(@PathVariable int id){
        ModelAndView modelAndView = new ModelAndView("redirect:/user/" + id);
        Optional<Person> currentPerson = getCurrentPerson();
        Optional<Person> personOptional = personService.findById(id);
        if(currentPerson.isPresent() && personOptional.isPresent()){
            Optional<Subscription> subscription = subscriptionService.findByPersonAndSubscriber(personOptional.get(), currentPerson.get());
            if(subscription.isPresent()){
                subscriptionService.deleteSubscription(subscription.get());
            } else {
                Subscription newSubscription = new Subscription();
                newSubscription.setPerson(personOptional.get());
                newSubscription.setSubscriber(currentPerson.get());
                subscriptionService.addSubscription(newSubscription);
            }
        }
        return modelAndView;
    }

    @PostMapping("/{id}/editrole")
    public ModelAndView changeUserRole(@PathVariable int id){
        ModelAndView modelAndView = new ModelAndView("redirect:/user/" + id);
        Optional<Person> personOptional = personService.findById(id);
        if(personOptional.isPresent()){
            if(personOptional.get().getRole_() == Role.USER){
                personOptional.get().setRole_(Role.MODERATOR);
                personService.editPerson(personOptional.get());
            } else {
                personOptional.get().setRole_(Role.USER);
                personService.editPerson(personOptional.get());
            }
        }
        return modelAndView;
    }

    @GetMapping("/notices")
    public ModelAndView getPurchaseNotices(){
        ModelAndView modelAndView;
        Optional<Person> currentPerson = getCurrentPerson();
        if(currentPerson.isPresent()){
            modelAndView = new ModelAndView("user-notices");
        } else {
            modelAndView = new ModelAndView("redirect:/");
            return modelAndView;
        }
        modelAndView.addObject("currentPerson", currentPerson);
        return modelAndView;
    }

    @PostMapping("/notices")
    public ModelAndView checkNotice(@RequestParam int noticeId){
        ModelAndView modelAndView;
        Optional<Person> currentPerson = getCurrentPerson();
        Optional<Purchasenotice> purchasenotice = purchasenoticeService.findById(noticeId);
        if(currentPerson.isPresent() && purchasenotice.isPresent()
                && purchasenotice.get().getPersonBean().getId() == currentPerson.get().getId()){
            modelAndView = new ModelAndView("redirect:/user/notices");
            if(purchasenotice.isPresent()){
                purchasenoticeService.checkNotice(purchasenotice.get());
            }
        } else {
            modelAndView = new ModelAndView("redirect:/");
            return modelAndView;
        }
        return modelAndView;
    }

    @GetMapping("/feed")
    public ModelAndView getFeed(){
        ModelAndView modelAndView;
        Optional<Person> currentPerson = getCurrentPerson();
        if(currentPerson.isEmpty()){
            modelAndView = new ModelAndView("redirect:/");
        } else {
            modelAndView = new ModelAndView("user-feed");
            modelAndView.addObject("currentPerson", currentPerson);
            List<Publication> feedList = publicationService.getFeedList(currentPerson.get());
            modelAndView.addObject("feedList", feedList);
        }
        return modelAndView;
    }

    @GetMapping("/wishlist")
    public ModelAndView getWishlist(){
        ModelAndView modelAndView = new ModelAndView("wishlist");
        Optional<Person> currentPerson = getCurrentPerson();
        modelAndView.addObject("wishList", wishItemService.getWishItemsByPerson(currentPerson.get()));
        modelAndView.addObject("currentPerson", currentPerson);
        return modelAndView;
    }

    public Optional<Person> getCurrentPerson(){
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        Optional<Person> currentPerson;
        if(authentication == null){
            currentPerson = Optional.empty();
        }
        else{
            currentPerson = personService.findByLogin(authentication.getName());
        }
        return currentPerson;
    }
}
