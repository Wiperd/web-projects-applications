package com.AnatoliiK.MuzCamp.Persistence.Service;

import com.AnatoliiK.MuzCamp.Persistence.Entity.Track;
import com.AnatoliiK.MuzCamp.Persistence.Repository.TrackRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.multipart.MultipartFile;

public interface TrackService {

    Track addTrack(Track track);
}
