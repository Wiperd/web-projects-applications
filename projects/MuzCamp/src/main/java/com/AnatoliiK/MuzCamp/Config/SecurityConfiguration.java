package com.AnatoliiK.MuzCamp.Config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.password.NoOpPasswordEncoder;

import javax.sql.DataSource;

@Configuration
@EnableWebSecurity
public class SecurityConfiguration extends WebSecurityConfigurerAdapter {
    @Autowired
    private DataSource dataSource;

    @Override
    public void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.jdbcAuthentication().dataSource(dataSource)
                .passwordEncoder(NoOpPasswordEncoder.getInstance())
                .usersByUsernameQuery("select login, password, 'true' from Person where login=?")
                .authoritiesByUsernameQuery("select login, role_ from Person where login=?");
    }

    @Override
    public void configure(HttpSecurity httpSecurity) throws Exception {
        httpSecurity
                .csrf()
                .disable()
                .authorizeRequests()
                .antMatchers("/signup", "/login").not().authenticated()
                .antMatchers("/user/settings", "/user/settings/**", "/user/{id}/xsubscribe", "/user/notices",
                        "/user/feed", "/user/wishlist", "/publication/create", "/publication/{id}/comment/post",
                        "/publication/{id}/comment/remove", "/publication/{id}/wish", "/publication/{id}/unwish").authenticated()
                .antMatchers("/user/{id}/editrole").hasAuthority("ADMIN")
                .and()
                .formLogin().loginPage("/login")
                .usernameParameter("login").defaultSuccessUrl("/")
                .failureUrl("/login?error=true").permitAll()
                .and()
                .logout().permitAll();
    }
}
