package com.AnatoliiK.MuzCamp.Persistence.Entity;

import javax.persistence.*;
import java.sql.Timestamp;

@Entity
@Table (name = "Purchasenotice")
public class Purchasenotice {
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    private Integer id;

    @Column(name="check_")
    private Boolean check;

    private String content;

    @Column(name="date_")
    private Timestamp date;

    @ManyToOne
    @JoinColumn(name="person")
    private Person personBean;

    @ManyToOne
    @JoinColumn(name="publication")
    private Publication publicationBean;

    public Purchasenotice() {
    }

    public Integer getId() {
        return this.id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Boolean getCheck() {
        return this.check;
    }

    public void setCheck(Boolean check) {
        this.check = check;
    }

    public String getContent() {
        return this.content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Timestamp getDate() {
        return this.date;
    }

    public void setDate(Timestamp date) {
        this.date = date;
    }

    public Person getPersonBean() {
        return this.personBean;
    }

    public void setPersonBean(Person personBean) {
        this.personBean = personBean;
    }

    public Publication getPublicationBean() {
        return this.publicationBean;
    }

    public void setPublicationBean(Publication publicationBean) {
        this.publicationBean = publicationBean;
    }
}
