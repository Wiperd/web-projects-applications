package com.AnatoliiK.MuzCamp.Persistence.Service;

import com.AnatoliiK.MuzCamp.Persistence.Entity.Collectible;
import com.AnatoliiK.MuzCamp.Persistence.Entity.Person;
import com.AnatoliiK.MuzCamp.Persistence.Entity.Publication;

import java.util.Optional;

public interface CollectibleService {
    Optional<Collectible> findByPublicationAndPerson(Publication publication, Person person);
    Collectible saveCollectible(Collectible collectible);
}
