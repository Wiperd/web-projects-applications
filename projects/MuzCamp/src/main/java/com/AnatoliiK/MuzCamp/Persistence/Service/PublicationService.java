package com.AnatoliiK.MuzCamp.Persistence.Service;

import com.AnatoliiK.MuzCamp.Persistence.Entity.Person;
import com.AnatoliiK.MuzCamp.Persistence.Entity.Publication;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Optional;

public interface PublicationService {
    Publication addPublication(Publication publication);
    Publication editPublication(Publication publication);
    Optional<Publication> findById(Integer id);
    Page<Publication> findByTitle(String title, Pageable pageable);
    List<Publication> findByTitle(String title);
    Page<Publication> findByPersonId(Integer personId, Pageable pageable);
    Page<Publication> findByGenreId(Integer genreId, Pageable pageable);
    Page<Object[]> findByGenreIdAndSortBySales(Integer genreId, Pageable pageable);
    Page<Object[]> getAllAndSortBySales(Pageable pageable);

    List<Publication> getFeedList(Person person);
}
