package com.AnatoliiK.MuzCamp.Persistence.Repository;

import com.AnatoliiK.MuzCamp.Persistence.Entity.Track;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TrackRepository extends JpaRepository<Track, Integer> {
}
