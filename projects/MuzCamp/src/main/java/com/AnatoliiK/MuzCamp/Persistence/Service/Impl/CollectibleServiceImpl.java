package com.AnatoliiK.MuzCamp.Persistence.Service.Impl;

import com.AnatoliiK.MuzCamp.Persistence.Entity.Collectible;
import com.AnatoliiK.MuzCamp.Persistence.Entity.Person;
import com.AnatoliiK.MuzCamp.Persistence.Entity.Publication;
import com.AnatoliiK.MuzCamp.Persistence.Repository.CollectibleRepository;
import com.AnatoliiK.MuzCamp.Persistence.Service.CollectibleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class CollectibleServiceImpl implements CollectibleService {
    @Autowired
    private CollectibleRepository collectibleRepository;

    @Override
    public Optional<Collectible> findByPublicationAndPerson(Publication publication, Person person) {
        return collectibleRepository.findByPublicationBeanAndPersonBean(publication, person);
    }

    @Override
    public Collectible saveCollectible(Collectible collectible) {
        return collectibleRepository.saveAndFlush(collectible);
    }
}
