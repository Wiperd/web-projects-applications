package com.AnatoliiK.MuzCamp.Persistence.Service.Impl;

import com.AnatoliiK.MuzCamp.Persistence.Entity.Person;
import com.AnatoliiK.MuzCamp.Persistence.Entity.Publication;
import com.AnatoliiK.MuzCamp.Persistence.Repository.PublicationRepository;
import com.AnatoliiK.MuzCamp.Persistence.Service.PublicationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
@Service
public class PublicationServiceImpl implements PublicationService {
    @Autowired
    private PublicationRepository publicationRepository;

    @Override
    public Publication addPublication(Publication publication) {
        return publicationRepository.saveAndFlush(publication);
    }

    @Override
    public Publication editPublication(Publication publication) {
        return publicationRepository.saveAndFlush(publication);
    }

    @Override
    public Optional<Publication> findById(Integer id) {
        return publicationRepository.findById(id);
    }

    @Override
    public Page<Publication> findByTitle(String title, Pageable pageable) {
        return publicationRepository.findByTitle(title, pageable);
    }

    @Override
    public List<Publication> findByTitle(String title) {
        return publicationRepository.findByTitle(title);
    }

    @Override
    public Page<Publication> findByPersonId(Integer personId, Pageable pageable) {
        return publicationRepository.findByPersonId(personId, pageable);
    }

    @Override
    public Page<Publication> findByGenreId(Integer genreId, Pageable pageable) {
        return publicationRepository.findByGenreId(genreId, pageable);
    }

    @Override
    public Page<Object[]> findByGenreIdAndSortBySales(Integer genreId, Pageable pageable) {
        return publicationRepository.findByGenreIdAndSortBySales(genreId, pageable);
    }

    @Override
    public Page<Object[]> getAllAndSortBySales(Pageable pageable) {
        return publicationRepository.getAllAndSortBySales(pageable);
    }

    @Override
    public List<Publication> getFeedList(Person person) {
        return publicationRepository.findBySubscriberOrderByDate(person.getId());
    }
}
