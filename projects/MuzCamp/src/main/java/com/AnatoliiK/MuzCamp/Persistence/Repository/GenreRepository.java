package com.AnatoliiK.MuzCamp.Persistence.Repository;

import com.AnatoliiK.MuzCamp.Persistence.Entity.Genre;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.Optional;

public interface GenreRepository extends JpaRepository<Genre, Integer> {
    @Query("SELECT G FROM Genre G")
    List<Genre> getAll();

    Optional<Genre> findById(Integer id);
}
