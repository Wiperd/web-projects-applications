package com.AnatoliiK.MuzCamp.Persistence.Service.Impl;

import com.AnatoliiK.MuzCamp.Persistence.Entity.Person;
import com.AnatoliiK.MuzCamp.Persistence.Entity.Publication;
import com.AnatoliiK.MuzCamp.Persistence.Entity.Subscription;
import com.AnatoliiK.MuzCamp.Persistence.Repository.SubscriptionRepository;
import com.AnatoliiK.MuzCamp.Persistence.Service.SubscriptionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class SubscriptionServiceImpl implements SubscriptionService {
    @Autowired
    private SubscriptionRepository subscriptionRepository;

    @Override
    public Subscription addSubscription(Subscription subscription) {
        return subscriptionRepository.saveAndFlush(subscription);
    }

    @Override
    public Optional<Subscription> findByPersonAndSubscriber(Person person, Person subscriber) {
        return subscriptionRepository.findByPersonAndSubscriber(person, subscriber);
    }

    @Override
    public Boolean deleteSubscription(Subscription subscription) {
        subscriptionRepository.delete(subscription);
        return true;
    }

}
