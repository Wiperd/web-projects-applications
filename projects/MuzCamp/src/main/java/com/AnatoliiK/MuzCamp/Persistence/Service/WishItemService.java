package com.AnatoliiK.MuzCamp.Persistence.Service;

import com.AnatoliiK.MuzCamp.Persistence.Entity.Person;
import com.AnatoliiK.MuzCamp.Persistence.Entity.Publication;
import com.AnatoliiK.MuzCamp.Persistence.Entity.WishItem;

import java.util.List;
import java.util.Optional;

public interface WishItemService {
    List<WishItem> getWishItemsByPerson(Person person);
    Optional<WishItem> getWishByPublicationAndPerson(Publication publication, Person person);
    WishItem addWishItemByPersonAndPublication(Person person, Publication publication);
    Boolean deleteWishItem(WishItem wishItem);
}
