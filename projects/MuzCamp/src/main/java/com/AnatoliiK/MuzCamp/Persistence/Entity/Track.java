package com.AnatoliiK.MuzCamp.Persistence.Entity;

import javax.persistence.*;

@Entity
@Table (name = "Track")
public class Track {
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    private Integer id;

    @Column(name="name_")
    private String name;


    @ManyToOne
    @JoinColumn(name="publication")
    private Publication publicationBean;

    public Track() {
    }

    public Integer getId() {
        return this.id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Publication getPublicationBean() {
        return this.publicationBean;
    }

    public void setPublicationBean(Publication publicationBean) {
        this.publicationBean = publicationBean;
    }
}
