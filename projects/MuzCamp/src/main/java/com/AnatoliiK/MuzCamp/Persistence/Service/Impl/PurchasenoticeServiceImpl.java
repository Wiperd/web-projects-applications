package com.AnatoliiK.MuzCamp.Persistence.Service.Impl;

import com.AnatoliiK.MuzCamp.Persistence.Entity.Purchasenotice;
import com.AnatoliiK.MuzCamp.Persistence.Repository.PurchasenoticeRepository;
import com.AnatoliiK.MuzCamp.Persistence.Service.PurchasenoticeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class PurchasenoticeServiceImpl implements PurchasenoticeService {
    @Autowired
    private PurchasenoticeRepository purchasenoticeRepository;

    @Override
    public Purchasenotice saveNotice(Purchasenotice purchasenotice) {
        return purchasenoticeRepository.saveAndFlush(purchasenotice);
    }

    @Override
    public Purchasenotice checkNotice(Purchasenotice purchasenotice) {
        purchasenotice.setCheck(true);
        return purchasenoticeRepository.saveAndFlush(purchasenotice);
    }

    @Override
    public Optional<Purchasenotice> findById(Integer id) {
        return purchasenoticeRepository.findById(id);
    }
}
