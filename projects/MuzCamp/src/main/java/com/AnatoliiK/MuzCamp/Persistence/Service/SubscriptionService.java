package com.AnatoliiK.MuzCamp.Persistence.Service;

import com.AnatoliiK.MuzCamp.Persistence.Entity.Person;
import com.AnatoliiK.MuzCamp.Persistence.Entity.Publication;
import com.AnatoliiK.MuzCamp.Persistence.Entity.Subscription;

import java.util.List;
import java.util.Optional;

public interface SubscriptionService {
    Subscription addSubscription(Subscription subscription);
    Optional<Subscription> findByPersonAndSubscriber(Person person, Person subscriber);

    Boolean deleteSubscription(Subscription subscription);
}
