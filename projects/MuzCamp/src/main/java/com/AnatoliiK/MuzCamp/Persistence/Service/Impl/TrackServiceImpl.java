package com.AnatoliiK.MuzCamp.Persistence.Service.Impl;

import com.AnatoliiK.MuzCamp.Persistence.Entity.Track;
import com.AnatoliiK.MuzCamp.Persistence.Repository.TrackRepository;
import com.AnatoliiK.MuzCamp.Persistence.Service.TrackService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

@Service
public class TrackServiceImpl implements TrackService {
    @Autowired
    private TrackRepository trackRepository;

    @Override
    public Track addTrack(Track track) {
        trackRepository.saveAndFlush(track);
        return track;
    }
}
