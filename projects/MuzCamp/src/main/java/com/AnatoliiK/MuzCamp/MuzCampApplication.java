package com.AnatoliiK.MuzCamp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;



@SpringBootApplication
public class MuzCampApplication {
	public static void main(String[] args) {
		SpringApplication.run(MuzCampApplication.class, args);
	}

}
