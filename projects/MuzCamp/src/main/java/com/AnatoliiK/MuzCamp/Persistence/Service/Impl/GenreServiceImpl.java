package com.AnatoliiK.MuzCamp.Persistence.Service.Impl;

import com.AnatoliiK.MuzCamp.Persistence.Entity.Genre;
import com.AnatoliiK.MuzCamp.Persistence.Repository.GenreRepository;
import com.AnatoliiK.MuzCamp.Persistence.Service.GenreService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class GenreServiceImpl implements GenreService {
    @Autowired
    GenreRepository genreRepository;

    @Override
    public List<Genre> getAll() {
        return genreRepository.getAll();
    }

    @Override
    public Optional<Genre> findById(Integer id) {
        return genreRepository.findById(id);
    }
}
