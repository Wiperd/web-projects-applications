package com.AnatoliiK.MuzCamp.Persistence.Entity;

import javax.persistence.*;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.List;

@Entity
@Table (name = "Publication")
public class Publication {
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    private Integer id;

    private Integer amount;

    @Column(name="date_")
    private Timestamp date;

    private String description;

    private Boolean merch;

    private Integer price;

    private String title;

    @OneToMany(mappedBy="publicationBean")
    private List<Track> tracks;

    @OneToMany(mappedBy="publicationBean")
    private List<Comment> comments;

    @ManyToOne
    @JoinColumn(name="image")
    private Image imageBean;

    @ManyToOne
    @JoinColumn(name="person")
    private Person personBean;

    @ManyToOne
    @JoinColumn(name="genre")
    private Genre genre;

    @OneToMany(mappedBy="publicationBean")
    private List<Purchasenotice> purchasenotices;

    @OneToMany(mappedBy="publicationBean")
    private List<Collectible> collectibles;

    @OneToMany(mappedBy="publicationBean")
    private List<WishItem> wishItems;

    public String getDateDDMMYYYY(){
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyy");
        return simpleDateFormat.format(getDate());
    }

    public Publication() {
    }

    public Integer getId() {
        return this.id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getAmount() {
        return this.amount;
    }

    public void setAmount(Integer amount) {
        this.amount = amount;
    }

    public Timestamp getDate() {
        return this.date;
    }

    public void setDate(Timestamp date) {
        this.date = date;
    }

    public String getDescription() {
        return this.description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Boolean getMerch() {
        return this.merch;
    }

    public void setMerch(Boolean merch) {
        this.merch = merch;
    }

    public Integer getPrice() {
        return this.price;
    }

    public void setPrice(Integer price) {
        this.price = price;
    }

    public String getTitle() {
        return this.title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public List<Track> getTracks() {
        return this.tracks;
    }

    public void setTracks(List<Track> tracks) {
        this.tracks = tracks;
    }

    public Track addTrack(Track track) {
        getTracks().add(track);
        track.setPublicationBean(this);

        return track;
    }

    public Track removeTrack(Track track) {
        getTracks().remove(track);
        track.setPublicationBean(null);

        return track;
    }

    public List<WishItem> getWishItems() {
        return this.wishItems;
    }

    public void setWishItems(List<WishItem> wishItems) {
        this.wishItems = wishItems;
    }

    public WishItem addWishItem(WishItem wishItem) {
        getWishItems().add(wishItem);
        wishItem.setPublicationBean(this);

        return wishItem;
    }

    public WishItem removeWishItem(WishItem wishItem) {
        getWishItems().remove(wishItem);
        wishItem.setPublicationBean(null);

        return wishItem;
    }

    public List<Comment> getComments() {
        return this.comments;
    }

    public void setComments(List<Comment> comments) {
        this.comments = comments;
    }

    public Comment addComment(Comment comment) {
        getComments().add(comment);
        comment.setPublicationBean(this);

        return comment;
    }

    public Comment removeComment(Comment comment) {
        getComments().remove(comment);
        comment.setPublicationBean(null);

        return comment;
    }

    public Image getImageBean() {
        return this.imageBean;
    }

    public void setImageBean(Image imageBean) {
        this.imageBean = imageBean;
    }

    public Person getPersonBean() {
        return this.personBean;
    }

    public void setPersonBean(Person personBean) {
        this.personBean = personBean;
    }

    public Genre getGenre(){return this.genre;}

    public void setGenre(Genre genre){
        this.genre = genre;
    }

    public List<Purchasenotice> getPurchasenotices() {
        return this.purchasenotices;
    }

    public void setPurchasenotices(List<Purchasenotice> purchasenotices) {
        this.purchasenotices = purchasenotices;
    }

    public Purchasenotice addPurchasenotice(Purchasenotice purchasenotice) {
        getPurchasenotices().add(purchasenotice);
        purchasenotice.setPublicationBean(this);

        return purchasenotice;
    }

    public Purchasenotice removePurchasenotice(Purchasenotice purchasenotice) {
        getPurchasenotices().remove(purchasenotice);
        purchasenotice.setPublicationBean(null);

        return purchasenotice;
    }

    public List<Collectible> getCollectibles() {
        return this.collectibles;
    }

    public void setCollectibles(List<Collectible> collectibles) {
        this.collectibles = collectibles;
    }

    public Collectible addCollectible(Collectible collectible) {
        getCollectibles().add(collectible);
        collectible.setPublicationBean(this);

        return collectible;
    }

    public Collectible removeCollectible(Collectible collectible) {
        getCollectibles().remove(collectible);
        collectible.setPublicationBean(null);

        return collectible;
    }
}
