package com.AnatoliiK.MuzCamp.Persistence.Service.Impl;

import com.AnatoliiK.MuzCamp.Persistence.Entity.Image;
import com.AnatoliiK.MuzCamp.Persistence.Repository.ImageRepository;
import com.AnatoliiK.MuzCamp.Persistence.Service.ImageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.sql.rowset.serial.SerialBlob;
import java.io.IOException;
import java.sql.SQLException;
import java.util.Optional;

@Service
public class ImageServiceImpl implements ImageService {
    @Autowired
    private ImageRepository imageRepository;


    @Override
    public Optional<Image> getImageById(int id) {
        return imageRepository.findById(id);
    }

    @Override
    public Image addImage(Image image) {
        return imageRepository.saveAndFlush(image);
    }

    @Override
    public Image addImageFromFile(MultipartFile file) throws Exception {
        Image image = new Image();
        image.setExtension(file.getOriginalFilename().split("\\.")[1]);
        image.setContent(new SerialBlob(file.getBytes()));
        return addImage(image);
    }
}
