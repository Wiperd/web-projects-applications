package com.AnatoliiK.MuzCamp.Persistence.Service;

import com.AnatoliiK.MuzCamp.Persistence.Entity.Purchasenotice;

import java.util.Optional;

public interface PurchasenoticeService {
    Purchasenotice saveNotice(Purchasenotice purchasenotice);
    Purchasenotice checkNotice(Purchasenotice purchasenotice);
    Optional<Purchasenotice> findById(Integer id);
}
