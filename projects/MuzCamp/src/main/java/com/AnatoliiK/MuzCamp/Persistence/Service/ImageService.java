package com.AnatoliiK.MuzCamp.Persistence.Service;

import com.AnatoliiK.MuzCamp.Persistence.Entity.Image;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.sql.SQLException;
import java.util.Optional;

public interface ImageService {
    Optional<Image> getImageById(int id);

    Image addImage(Image image);
    Image addImageFromFile(MultipartFile file) throws Exception;
}
