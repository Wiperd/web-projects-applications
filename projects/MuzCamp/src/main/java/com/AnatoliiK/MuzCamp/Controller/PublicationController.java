package com.AnatoliiK.MuzCamp.Controller;


import com.AnatoliiK.MuzCamp.Persistence.Entity.*;
import com.AnatoliiK.MuzCamp.Persistence.Role;
import com.AnatoliiK.MuzCamp.Persistence.Service.Impl.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.parameters.P;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import java.io.File;
import java.io.IOException;
import java.sql.Array;
import java.sql.Timestamp;
import java.util.List;
import java.util.Optional;

@Controller
@RequestMapping("/publication")
public class PublicationController {
    @Autowired
    private PublicationServiceImpl publicationService;

    @Autowired
    private TrackServiceImpl trackService;

    @Autowired
    private PersonServiceImpl personService;

    @Autowired
    private ImageServiceImpl imageService;

    @Autowired
    private CollectibleServiceImpl collectibleService;

    @Autowired
    private CommentServiceImpl commentService;

    @Autowired
    private PurchasenoticeServiceImpl purchasenoticeService;

    @Autowired
    private GenreServiceImpl genreService;

    @Autowired
    private WishItemServiceImpl wishItemService;

    private final String trackDir = "C:\\some\\trackdir\\";

    @GetMapping("/{id}")
    public ModelAndView getPublicationMain(@PathVariable(name = "id") int id){
        ModelAndView modelAndView;
        Optional<Publication> publication = publicationService.findById(id);
        if(publication.isEmpty()){
            modelAndView = new ModelAndView("redirect:/");
            return modelAndView;
        } else {
            modelAndView = new ModelAndView("publication-main");
            modelAndView.addObject("publication", publication.get());
        }
        Optional<Person> currentPerson = getCurrentPerson();
        modelAndView.addObject("currentPerson", currentPerson);
        boolean canBuy;
        if (currentPerson.isPresent()
                && ( collectibleService.findByPublicationAndPerson(publication.get(), currentPerson.get()).isPresent()
                || publication.get().getPersonBean().getId() == currentPerson.get().getId() )
                || currentPerson.isEmpty() && !publication.get().getMerch()
                || publication.get().getMerch() && publication.get().getAmount() == 0){
            canBuy = false;
        } else{
            canBuy = true;
        }
        modelAndView.addObject("canBuy", canBuy);

        boolean canReview;
        if(currentPerson.isPresent()
                && collectibleService.findByPublicationAndPerson(publication.get(), currentPerson.get()).isPresent()
                && commentService.findCommentByPublicationAndPerson(publication.get(), currentPerson.get()).isEmpty()
                && publication.get().getPersonBean().getId() != currentPerson.get().getId()){
            canReview = true;
        } else {
            canReview = false;
        }
        modelAndView.addObject("canReview", canReview);

        boolean wishListed;
        if(currentPerson.isPresent()
                && wishItemService.getWishByPublicationAndPerson(publication.get(), currentPerson.get()).isPresent()
                && canBuy)
        {
            wishListed = true;
        } else {
            wishListed = false;
        }
        modelAndView.addObject("wishListed", wishListed);
        return modelAndView;
    }

    @GetMapping("/{id}/buy")
    public ModelAndView getBuyForm(@PathVariable(name = "id") int id){
        ModelAndView modelAndView = new ModelAndView("publication-buy");
        Optional<Person> currentPerson = getCurrentPerson();
        Optional<Publication> publication = publicationService.findById(id);
        modelAndView.addObject("purchasenotice", new Purchasenotice());
        modelAndView.addObject("publication", publication.get());
        modelAndView.addObject("currentPerson", currentPerson);
        return modelAndView;
    }

    @PostMapping("/{id}/buy")
    public ModelAndView buyPublication(@PathVariable(name = "id") int id, @ModelAttribute Purchasenotice purchasenotice,
                                       @RequestParam(name = "cardNumber") String cardNumber, @RequestParam String cardCVC,
                                       @RequestParam String cardMonth, @RequestParam String cardYear){
        ModelAndView modelAndView = new ModelAndView("redirect:/");
        Optional<Publication> publication = publicationService.findById(id);
        Optional<Person> currentPerson = getCurrentPerson();
        boolean purchaseCompleted = validateTransaction(cardNumber, cardCVC, cardYear, cardMonth);
        if(purchaseCompleted){
            if(publication.get().getMerch()){
                purchasenotice.setPersonBean(publication.get().getPersonBean());
                purchasenotice.setPublicationBean(publication.get());
                purchasenotice.setDate(new Timestamp(System.currentTimeMillis()));
                purchasenotice.setCheck(false);
                purchasenoticeService.saveNotice(purchasenotice);
                publication.get().setAmount(publication.get().getAmount() - 1);
                publicationService.editPublication(publication.get());
            }
            if (currentPerson.isPresent()){
                Collectible collectible = new Collectible();
                collectible.setPersonBean(currentPerson.get());
                collectible.setPublicationBean(publication.get());
                collectibleService.saveCollectible(collectible);
                Optional<WishItem> wishItemOptional = wishItemService.getWishByPublicationAndPerson(publication.get(), currentPerson.get());
                if(wishItemOptional.isPresent()){
                    wishItemService.deleteWishItem(wishItemOptional.get());
                }
            }
            if(publication.get().getPersonBean().getPaycheck() == null){
                publication.get().getPersonBean().setPaycheck(publication.get().getPrice());
            } else {
                publication.get().getPersonBean().setPaycheck(publication.get().getPersonBean().getPaycheck() + publication.get().getPrice());
            }
            personService.editPerson(publication.get().getPersonBean());
        }
        return modelAndView;
    }

    @PostMapping("/{id}/comment/post")
    public ModelAndView postReview(@PathVariable(name = "id") int id, @RequestParam(name = "commentInput") String commentInput){
        ModelAndView modelAndView = new ModelAndView("redirect:/publication/" + id);
        Optional<Publication> publication = publicationService.findById(id);
        Optional<Person> currentPerson = getCurrentPerson();
        Comment comment = new Comment();
        comment.setText(commentInput);
        comment.setDate(new Timestamp(System.currentTimeMillis()));
        comment.setPersonBean(currentPerson.get());
        comment.setPublicationBean(publication.get());
        commentService.addComment(comment);
        return modelAndView;
    }

    @PostMapping("/{id}/comment/remove")
    public ModelAndView deleteReview(@PathVariable(name = "id") int id, @RequestParam(name = "commentId") int commentId){
        ModelAndView modelAndView = new ModelAndView("redirect:/publication/" + id);
        Optional<Person> currentPerson = getCurrentPerson();
        Optional<Comment> comment = commentService.findCommentById(commentId);
        if (currentPerson.isPresent() && comment.isPresent()
                && (currentPerson.get().getId() == comment.get().getPersonBean().getId()
                    || currentPerson.get().getRole_() == Role.ADMIN
                    || currentPerson.get().getRole_() == Role.MODERATOR)){
            commentService.deleteById(commentId);
        }
        return modelAndView;
    }

    @GetMapping("/create")
    public ModelAndView getPublicationForm(@RequestParam("merch") Boolean merch) {
        ModelAndView modelAndView = new ModelAndView("publication-create");
        Optional<Person> currentPerson = getCurrentPerson();
        Publication publication = new Publication();
        publication.setPersonBean(currentPerson.get());
        publication.setMerch(merch);
        modelAndView.addObject("publication", publication);
        modelAndView.addObject("genres", genreService.getAll());
        modelAndView.addObject("currentPerson", currentPerson);
        return modelAndView;
    }

    @PostMapping("/create")
    public ModelAndView createPublication(@ModelAttribute Publication publication,
                                          @RequestParam(name = "image", required = false) MultipartFile image,
                                          @RequestParam(name = "trackFiles", required = false) MultipartFile[] trackFiles,
                                          @RequestParam(name = "trackNames", required = false) String[] trackNames,
                                          @ModelAttribute(name = "genre") Integer genre) {
        ModelAndView modelAndView;
        Optional<Person> currentPerson = getCurrentPerson();

        if(image != null && !image.isEmpty()){
            try {
                publication.setImageBean(imageService.addImageFromFile(image));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        if(genre != 0){
            publication.setGenre(genreService.findById(genre).get());
        }
        publication.setPersonBean(currentPerson.get());
        publication.setDate(new Timestamp(System.currentTimeMillis()));
        publicationService.addPublication(publication);

        if (trackFiles != null){
            for (int i = 0; i < trackFiles.length; i++) {
                if (trackFiles[i] != null){
                    Track track = new Track();
                    track.setPublicationBean(publication);
                    track.setName(trackNames[i]);
                    trackService.addTrack(track);
                    try {
                        trackFiles[i].transferTo(new File(trackDir + track.getId() + ".mp3"));
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
        modelAndView = new ModelAndView("redirect:/user/" + currentPerson.get().getId() + "/publications");
        return modelAndView;
    }

    @PostMapping("/{id}/wish")
    public ModelAndView addWish(@PathVariable(name = "id") int id){
        ModelAndView modelAndView = new ModelAndView("redirect:/publication/" + id);
        Optional<Person> currentPerson = getCurrentPerson();
        Optional<Publication> publication = publicationService.findById(id);
        if(currentPerson.isPresent() && publication.isPresent()){
            wishItemService.addWishItemByPersonAndPublication(currentPerson.get(), publication.get());
        }
        return modelAndView;
    }

    @PostMapping("/{id}/unwish")
    public ModelAndView removeWish(@PathVariable(name = "id") int id){
        ModelAndView modelAndView = new ModelAndView("redirect:/user/wishlist");
        Optional<Person> currentPerson = getCurrentPerson();
        Optional<Publication> publication = publicationService.findById(id);
        if(currentPerson.isPresent() && publication.isPresent()){
            Optional<WishItem> wishItemOptional = wishItemService.getWishByPublicationAndPerson(publication.get(), currentPerson.get());
            if(wishItemOptional.isPresent()){
                wishItemService.deleteWishItem(wishItemOptional.get());
            }
        }
        return modelAndView;
    }

    public boolean validateTransaction(String cardNumber, String cardCVC, String cardYear, String cardMonth){
        boolean validated = true;
        //validate
        return validated;
    }

    public Optional<Person> getCurrentPerson() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        Optional<Person> currentPerson;
        if (authentication == null) {
            currentPerson = Optional.empty();
        } else {
            currentPerson = personService.findByLogin(authentication.getName());
        }
        return currentPerson;
    }
}
