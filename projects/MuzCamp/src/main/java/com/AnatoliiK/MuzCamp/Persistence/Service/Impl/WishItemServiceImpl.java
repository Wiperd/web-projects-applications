package com.AnatoliiK.MuzCamp.Persistence.Service.Impl;

import com.AnatoliiK.MuzCamp.Persistence.Entity.Person;
import com.AnatoliiK.MuzCamp.Persistence.Entity.Publication;
import com.AnatoliiK.MuzCamp.Persistence.Entity.WishItem;
import com.AnatoliiK.MuzCamp.Persistence.Repository.WishItemRepository;
import com.AnatoliiK.MuzCamp.Persistence.Service.WishItemService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class WishItemServiceImpl implements WishItemService {
    @Autowired
    private WishItemRepository wishItemRepository;


    @Override
    public List<WishItem> getWishItemsByPerson(Person person) {
        return wishItemRepository.findByPersonBean(person);
    }

    @Override
    public Optional<WishItem> getWishByPublicationAndPerson(Publication publication, Person person) {
        return wishItemRepository.findByPublicationBeanAndPersonBean(publication, person);
    }

    @Override
    public WishItem addWishItemByPersonAndPublication(Person person, Publication publication) {
        WishItem wishItem = new WishItem();
        wishItem.setPersonBean(person);
        wishItem.setPublicationBean(publication);
        wishItemRepository.saveAndFlush(wishItem);
        return wishItem;
    }

    @Override
    public Boolean deleteWishItem(WishItem wishItem) {
        wishItemRepository.delete(wishItem);
        return true;
    }
}
