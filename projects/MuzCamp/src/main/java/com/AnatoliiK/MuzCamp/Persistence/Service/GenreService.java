package com.AnatoliiK.MuzCamp.Persistence.Service;

import com.AnatoliiK.MuzCamp.Persistence.Entity.Genre;

import java.util.List;
import java.util.Optional;

public interface GenreService {
    List<Genre> getAll();
    Optional<Genre> findById(Integer id);
}
