package com.AnatoliiK.MuzCamp.Persistence.Repository;

import com.AnatoliiK.MuzCamp.Persistence.Entity.Collectible;
import com.AnatoliiK.MuzCamp.Persistence.Entity.Person;
import com.AnatoliiK.MuzCamp.Persistence.Entity.Publication;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface CollectibleRepository extends JpaRepository<Collectible, Integer> {

    Optional<Collectible> findByPublicationBeanAndPersonBean(Publication publication, Person person);
}
