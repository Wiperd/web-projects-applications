package com.AnatoliiK.MuzCamp.Persistence.Entity;

import javax.persistence.*;

@Entity
@Table (name = "Genre")
public class Genre {
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    private Integer id;

    @Column(name="name_")
    private String name;

    public Genre() {
    }

    public Integer getId() {
        return this.id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
