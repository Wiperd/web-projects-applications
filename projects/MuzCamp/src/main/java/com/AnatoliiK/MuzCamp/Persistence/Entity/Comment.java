package com.AnatoliiK.MuzCamp.Persistence.Entity;

import javax.persistence.*;
import java.sql.Timestamp;

@Entity
@Table (name = "Comment")
public class Comment {
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    private Integer id;

    @Column(name="date_")
    private Timestamp date;

    private String text;

    @ManyToOne
    @JoinColumn(name="person")
    private Person personBean;

    @ManyToOne
    @JoinColumn(name="publication")
    private Publication publicationBean;

    public Comment() {
    }

    public Integer getId() {
        return this.id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Timestamp getDate() {
        return this.date;
    }

    public void setDate(Timestamp date) {
        this.date = date;
    }

    public String getText() {
        return this.text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Person getPersonBean() {
        return this.personBean;
    }

    public void setPersonBean(Person personBean) {
        this.personBean = personBean;
    }

    public Publication getPublicationBean() {
        return this.publicationBean;
    }

    public void setPublicationBean(Publication publicationBean) {
        this.publicationBean = publicationBean;
    }

}
