package com.AnatoliiK.MuzCamp.Controller;

import com.AnatoliiK.MuzCamp.Persistence.Entity.Image;
import com.AnatoliiK.MuzCamp.Persistence.Service.Impl.ImageServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/image")
public class ImageController {
    @Autowired
    ImageServiceImpl imageService;

    @RequestMapping(value = "{id}")
    @ResponseBody
    public ResponseEntity<byte []> downloadUserAvatarImage(@PathVariable int id) throws Exception{
        Image image = imageService.getImageById(id).get();
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(getMediaType(image.getExtension()));

        return new ResponseEntity<byte[]>(image.getContent().getBinaryStream().readAllBytes(), headers, HttpStatus.CREATED);
    }

    MediaType getMediaType(String extension){
        if(extension.equals("jpg") || extension.equals("jpeg")){
            return MediaType.IMAGE_JPEG;
        } else {
            return MediaType.IMAGE_PNG;
        }
    }
}
