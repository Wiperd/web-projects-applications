package com.AnatoliiK.MuzCamp.Persistence.Repository;

import com.AnatoliiK.MuzCamp.Persistence.Entity.Person;
import com.AnatoliiK.MuzCamp.Persistence.Entity.Publication;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.Optional;

public interface PublicationRepository extends JpaRepository<Publication, Integer> {
    Optional<Publication> findById(Integer id);
    Page<Publication> findByTitle(String title, Pageable pageable);
    List<Publication> findByTitle(String title);

    @Query("SELECT P FROM Publication P WHERE P.personBean.id = :personId ")
    Page<Publication> findByPersonId(@Param("personId") Integer personId, Pageable pageable);

    @Query("SELECT P FROM Publication P WHERE P.genre.id = :genreId")
    Page<Publication> findByGenreId(@Param("genreId") Integer genreId, Pageable pageable);

    @Query("SELECT P, COUNT(C.id) as sales_count FROM Publication P LEFT JOIN Collectible C " +
            "ON C.publicationBean.id = P.id WHERE P.genre.id = :genreId GROUP BY P.id")
    Page<Object[]> findByGenreIdAndSortBySales(@Param("genreId") Integer genreId, Pageable pageable);

    @Query("SELECT P, COUNT(C.id) as sales_count FROM Publication P LEFT JOIN Collectible C " +
            "ON C.publicationBean.id = P.id GROUP BY P.id ORDER BY sales_count DESC")
    Page<Object[]> getAllAndSortBySales(Pageable pageable);

    @Query("SELECT P FROM Publication P " +
            "INNER JOIN Subscription S ON P.personBean.id = S.person.id " +
            "WHERE S.subscriber.id = :subscriberId " +
            "ORDER BY P.date DESC")
    List<Publication> findBySubscriberOrderByDate(@Param("subscriberId") Integer subscriberId);
}
