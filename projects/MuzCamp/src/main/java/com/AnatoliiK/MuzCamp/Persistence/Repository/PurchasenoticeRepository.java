package com.AnatoliiK.MuzCamp.Persistence.Repository;

import com.AnatoliiK.MuzCamp.Persistence.Entity.Purchasenotice;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PurchasenoticeRepository extends JpaRepository<Purchasenotice, Integer> {
}
