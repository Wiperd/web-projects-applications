package com.AnatoliiK.MuzCamp.Persistence.Entity;


import javax.persistence.*;

@Entity
@Table (name = "Subscription")
public class Subscription {
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    private Integer id;

    @ManyToOne
    @JoinColumn(name="person")
    private Person person;

    @ManyToOne
    @JoinColumn(name="subscriber")
    private Person subscriber;

    public Subscription() {
    }

    public Integer getId() {
        return this.id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Person getPerson() {
        return this.person;
    }

    public void setPerson(Person person) {
        this.person = person;
    }

    public Person getSubscriber() {
        return this.subscriber;
    }

    public void setSubscriber(Person subscriber) {
        this.subscriber = subscriber;
    }
}
