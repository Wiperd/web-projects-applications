package com.AnatoliiK.MuzCamp.Persistence;

public enum Role {
    USER,
    MODERATOR,
    ADMIN
}
