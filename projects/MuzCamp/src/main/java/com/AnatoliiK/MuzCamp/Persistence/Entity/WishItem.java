package com.AnatoliiK.MuzCamp.Persistence.Entity;

import javax.persistence.*;

@Entity
@Table (name = "Wishitem")
public class WishItem {
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    private Integer id;

    @ManyToOne
    @JoinColumn(name="person")
    private Person personBean;

    @ManyToOne
    @JoinColumn(name="publication")
    private Publication publicationBean;

    public WishItem() {
    }

    public Integer getId() {
        return this.id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Person getPersonBean() {
        return this.personBean;
    }

    public void setPersonBean(Person personBean) {
        this.personBean = personBean;
    }

    public Publication getPublicationBean() {
        return this.publicationBean;
    }

    public void setPublicationBean(Publication publicationBean) {
        this.publicationBean = publicationBean;
    }
}
