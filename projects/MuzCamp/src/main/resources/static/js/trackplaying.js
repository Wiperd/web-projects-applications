function playTrack(id) {
    var label = document.getElementById("trackLabel");
    var trackName = document.getElementById("track" + id);
    var trackSrc = document.getElementById("trackSource");
    var player = document.getElementById("audioPlayer");

    label.innerText = trackName.innerText;
    trackSrc.src = `/track/` + id;

    player.load();
    player.play();
}