/*
SET FOREIGN_KEY_CHECKS = 0;
DROP TABLE IF EXISTS Image;
DROP TABLE IF EXISTS Genre;
DROP TABLE IF EXISTS Person;
DROP TABLE IF EXISTS Publication;
DROP TABLE IF EXISTS Track;
DROP TABLE IF EXISTS Comment;
DROP TABLE IF EXISTS PurchaseNotice;
DROP TABLE IF EXISTS Collectible;
DROP TABLE IF EXISTS Subscription;
DROP TABLE IF EXISTS WishItem;
SET FOREIGN_KEY_CHECKS = 1;
*/
CREATE TABLE IF NOT EXISTS Image
(
    id INT NOT NULL AUTO_INCREMENT,
    content MEDIUMBLOB NOT NULL ,
    extension VARCHAR(8) NOT NULL ,
    PRIMARY KEY (id)
    );

CREATE TABLE IF NOT EXISTS Genre
(
    id INT NOT NULL,
    name_ VARCHAR(20) NOT NULL UNIQUE ,
    PRIMARY KEY (id)
    );

INSERT IGNORE INTO Genre (id, name_) VALUES (1, 'Electronic');
INSERT IGNORE INTO Genre (id, name_) VALUES (2, 'Rock');
INSERT IGNORE INTO Genre (id, name_) VALUES (3, 'Jazz');
INSERT IGNORE INTO Genre (id, name_) VALUES (4, 'Pop');
INSERT IGNORE INTO Genre (id, name_) VALUES (5, 'Hip-hop');
INSERT IGNORE INTO Genre (id, name_) VALUES (6, 'Classical');

CREATE TABLE IF NOT EXISTS Person
(
    id INT NOT NULL AUTO_INCREMENT,
    username VARCHAR(50) NOT NULL,
    login VARCHAR(30) NOT NULL UNIQUE,
    password VARCHAR(30) NOT NULL,
    paycheck INT,
    role_ VARCHAR(15) NOT NULL CHECK ( role_ IN ('USER', 'MODERATOR', 'ADMIN')),
    information VARCHAR(500),
    image INT,
    FOREIGN KEY (image) REFERENCES Image(id),
    PRIMARY KEY (id)
    );


CREATE TABLE IF NOT EXISTS Publication
(
    id INT NOT NULL AUTO_INCREMENT,
    title VARCHAR(60) NOT NULL,
    description VARCHAR(500),
    date_ TIMESTAMP DEFAULT CURRENT_TIMESTAMP ,
    merch BIT NOT NULL,
    price INT NOT NULL,
    amount INT(10),
    image INT,
    genre INT,
    person INT NOT NULL,
    FOREIGN KEY (person) REFERENCES Person(id),
    FOREIGN KEY (image) REFERENCES Image(id),
    FOREIGN KEY (genre) REFERENCES Genre(id),
    PRIMARY KEY (id)
    );

CREATE TABLE IF NOT EXISTS Track
(
    id INT NOT NULL AUTO_INCREMENT,
    name_ VARCHAR(50) NOT NULL ,
    publication INT NOT NULL,
    FOREIGN KEY (publication) REFERENCES Publication(id),
    PRIMARY KEY (id)
    );

CREATE TABLE IF NOT EXISTS Comment
(
    id INT NOT NULL AUTO_INCREMENT,
    text VARCHAR(500) NOT NULL,
    date_ TIMESTAMP DEFAULT CURRENT_TIMESTAMP ,
    person INT NOT NULL,
    publication INT NOT NULL,
    FOREIGN KEY (person) REFERENCES Person(id),
    FOREIGN KEY (publication) REFERENCES Publication(id),
    PRIMARY KEY (id)
    );

CREATE TABLE IF NOT EXISTS PurchaseNotice
(
    id INT NOT NULL AUTO_INCREMENT,
    content VARCHAR(2000) NOT NULL ,
    date_ TIMESTAMP DEFAULT CURRENT_TIMESTAMP ,
    check_ BIT NOT NULL ,
    person INT NOT NULL,
    publication INT NOT NULL,
    FOREIGN KEY (person) REFERENCES Person(id),
    FOREIGN KEY (publication) REFERENCES Publication(id),
    PRIMARY KEY (id)
    );


CREATE TABLE IF NOT EXISTS Collectible
(
    id INT NOT NULL AUTO_INCREMENT,
    person INT NOT NULL,
    publication INT NOT NULL,
    FOREIGN KEY (person) REFERENCES Person(id),
    FOREIGN KEY (publication) REFERENCES Publication(id),
    PRIMARY KEY (id)
    );


CREATE TABLE IF NOT EXISTS Subscription
    (
        id INT NOT NULL AUTO_INCREMENT,
        person INT NOT NULL,
        subscriber INT NOT NULL,
        FOREIGN KEY (person) REFERENCES Person(id),
        FOREIGN KEY (subscriber) REFERENCES Person(id),
        PRIMARY KEY (id)
);


CREATE TABLE IF NOT EXISTS WishItem
(
    id INT NOT NULL AUTO_INCREMENT,
    person INT NOT NULL,
    publication INT NOT NULL,
    FOREIGN KEY (person) REFERENCES Person(id),
    FOREIGN KEY (publication) REFERENCES Publication(id),
    PRIMARY KEY (id)
);
